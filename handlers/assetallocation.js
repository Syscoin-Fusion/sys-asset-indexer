const { models } = require('../db')

module.exports = (entry, cb) => {
    // Deletes _id property from response so it doesnt crash mongodb.
    delete entry._id

    // Indexes transaction into DB.
    models.AssetAllocation.create(entry).then(() => cb()).catch(e => cb(e))
}
