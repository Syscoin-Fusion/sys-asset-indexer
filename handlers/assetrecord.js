const { models } = require('../db')

module.exports = (entry, cb) => {
    // Rename and delete _id property from response so it doesnt crash mongodb.
    entry.asset_id = entry._id
    delete entry._id

    // Indexes asset into DB.
    models.AssetRecord.create(entry).then(() => cb()).catch(e => cb(e))
}
