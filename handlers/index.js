const assetallocation = require('./assetallocation')
const assetrecord = require('./assetrecord')

module.exports = {
    assetallocation,
    assetrecord
}
