const queue = require('kue').createQueue()
const handlers = require('./handlers')

module.exports = () => {
    // Process the queues
    queue.process('assetallocation', 5, (job, done) => {
        const data = job.data

        handlers.assetallocation(data, (err) => {
            if (err) {
                return done(err)
            }

            return done()
        })
    })

    queue.process('assetrecord', 5, (job, done) => {
        const data = job.data

        handlers.assetrecord(data, (err) => {
            if (err) {
                return done(err)
            }

            return done()
        })
    })
}
