const AssetRecord = require('./assetrecord')
const AssetAllocation = require('./assetallocation')

module.exports = {
    AssetRecord,
    AssetAllocation
}
