const mongoose = require('mongoose')

const AssetRecordSchema = new mongoose.Schema({
    asset_id: String,
    guid: String,
    symbol: String,
    category: String,
    alias: String,
    height: Number,
    use_input_ranges: Boolean,
    precision: Number,
    balance: Number,
    total_supply: Number,
    max_supply: Number,
    interest_rate: Number
})

module.exports = mongoose.model('assetrecord', AssetRecordSchema)
