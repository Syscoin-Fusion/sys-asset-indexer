const mongoose = require('mongoose')

const AssetRecordSchema = new mongoose.Schema({
    txid: String,
    time: Number,
    asset: String,
    symbol: String,
    interes_rate: Number,
    height: Number,
    sender: String,
    sender_balance: String,
    receiver: String,
    receiver_balance: String,
    memo: String,
    confirmed: Boolean,
    amount: String
})

module.exports = mongoose.model('assetallocation', AssetRecordSchema)
