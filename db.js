
const db = require('mongoose').connect(process.env.MONGO_URL + ':' + process.env.MONGO_PORT, (err) => {
    if (err) {
        throw err
    }

    models.AssetAllocation.collection.drop()
    models.AssetRecord.collection.drop()
})
const models = require('./models')

module.exports = {
    db,
    models
}
