require('dotenv').config()

const socket = require('zeromq').socket('sub')
const kue = require('kue')
const queueProcessor = require('./queues')
const queue = kue.createQueue() // Initialize job queue

// connects to ZMQ server
socket.connect(process.env.ZMQ_LISTEN)

// subscribe to certain events
socket.subscribe('assetallocation')
socket.subscribe('assetrecord')
socket.subscribe('aliasrecord')

socket.on('message', (a, b) => {
    const command = a.toString() // Event name
    const entry = JSON.parse(b.toString()) // Event data

    switch (command) {
        // Put the items into queue
        case 'assetallocation':
            queue.create('assetallocation', entry).attempts(20).save()
            break
        case 'assetrecord':
            queue.create('assetrecord', entry).attempts(20).save()
            break
        case 'asliasrecord':
            console.log(entry)
            break
    }
})

queueProcessor()
